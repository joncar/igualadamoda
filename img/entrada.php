<?php

if (!empty($_GET['code'])) {
    //$codigo = base64_decode($_GET['code']);
    $codigo = '';
    $image = ImageCreateFromJPEG("entrada.jpg");
    $color = imagecolorallocate($image, 0, 0, 0);
    $font = '../fonts/Montserrat.ttf';
    $a = imagettftextjustified($image, 12, -45, 330, 40, $color, $font, $codigo);
    header('Content-type: image/jpeg');
    imagejpeg($image, NULL, 100);
}

function imagettftextjustified(&$image, $size, $angle, $left, $top, $color, $font, $text) {
    imagettftext($image, $size, $angle, $left, $top, $color, $font, $text);
}
