<?php 
require_once APPPATH.'/controllers/Panel.php';    
class Frontend extends Main{        
    function __construct() {
        parent::__construct();
        $this->load->model('querys');
        $this->load->library('form_validation');
    }        

    function reservas(){
        $this->load->library('grocery_crud');
        $this->load->library('ajax_grocery_crud');
        $crud = new ajax_grocery_CRUD();
        $crud->set_table('reservas');
        $crud->set_subject('Reservas');
        $crud->set_theme('bootstrap2');
        $crud->display_as('talleres_id','Workshops id')
             ->display_as('taller','Workshops')
             ->display_as('nombre','Nom')
             ->display_as('telefono','Telèfon');   
        $crud->required_fields_array();
        $crud->callback_before_insert(function($post){
            unset($post['condiciones']);
            return $post;
        });
        $crud->callback_before_insert(function($post){
            $talleres = get_instance()->db->get_where('talleres',array('id'=>$post['talleres_id']));
            if($talleres->num_rows()>0){
                $precio = $talleres->row()->precio;
                $post['precio'] = $post['cantidad']*$precio;
                $post['fecha_reserva'] = date("Y-m-d H:i:s");
                return $post;
            }else{
                return false;
            }
        });
        $crud->callback_after_insert(function($post,$primary){
            $randon = 'IM-'.date("dism").str_replace('=','3',base64_encode($primary));            
            get_instance()->db->update('reservas',array('codigo_reserva'=>$randon),array('id'=>$primary));
            get_instance()->enviarcorreo((object)$post,2,'info@igualadamoda.com');
            get_instance()->enviarcorreo((object)$post,3);            
        });
        $crud->set_rules('email','Email','required|valid_email|is_unique[reservas.email]');
        //$crud->set_rules('talleres_id','Taller','required|callback_validate_condiciones');
        $crud = $crud->render();
        $this->loadView($crud);
    }
    
    function validate_condiciones(){
        if(empty($_POST['condiciones'])){
            $this->form_validation->set_message('validate_condiciones','Debe aceptar las condiciones de uso');
            return false;
        }
    }
}
