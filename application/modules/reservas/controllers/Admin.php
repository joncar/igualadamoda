<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }
        
        function talleres($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->set_subject('Workshop');
            $crud = $crud->render();
            $crud->title = "Workshops";
            $this->loadView($crud);
        }
        
        function reservas($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            if(is_numeric($x)){
                $crud->where('YEAR(fecha_reserva)',$x);
            }else{
                $crud->where('YEAR(fecha_reserva)',date("Y"));
            }
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar entrada','',base_url('reservas/admin/enviar_invitacion').'/');
            $crud->order_by('id','DESC');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function enviar_invitacion($reserva){
            $reserva = $this->db->get_where('reservas',array('id'=>$reserva));
            if($reserva->num_rows()>0){
                $reserva = $reserva->row();
                if($reserva->pagado==0){
                    echo "Reserva encara no cobrada <a href='".base_url('reservas/admin/reservas')."'>Tornar</a>";
                }else{
                    $reserva->image = $this->load->view('entrada',array('reserva'=>$reserva),TRUE);
                    
                    $this->load->library('mailer');                    
                    $entrada = file_get_contents(base_url('img/entrada.php?code='.base64_encode($reserva->codigo_reserva)));
                    file_put_contents('entrada.jpg',$entrada);
                    //$this->mailer->mail->AddEmbeddedImage('entrada.jpg','entrada');
                    $this->enviarcorreo($reserva,4);
                    echo "INVITACIÓ ENVIADA AMB ÈXIT";
                }
            }else{
                echo "Reserva no trobada <a href='".base_url('reservas/admin/reservas')."'>Tornar</a>";
            }
        }
    }
?>
