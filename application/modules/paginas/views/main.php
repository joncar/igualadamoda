<!-- ## HEADER ## -->
<header id="qcHomeHeader">
    <div class="row">

        <!-- ## LOGO ## -->
        <div id="qcLogo" class="col-6 col">
            <a href="index.html"><img src="<?= base_url() ?>img/placeholder/logo-header.png" alt="" /></a>
        </div>

        <!-- ## SITE NAVIGATION ## -->
        <nav id="qcPriNav" class="col-6 col">
            <ul class="clearfix">
                <li><a href="<?= site_url('p/workshops') ?>"><i class="icon-calendar-2 icon"></i> <span>WORKSHOP</span></a></li>
                <li><a href="<?= site_url('p/reserva') ?>"><i class="icon-ticket icon"></i> <span>RESERVA</span></a></li>
            </ul>
        </nav>

    </div>
    <div class="row">

        <!-- ## COUNTDOWN TIMER ## -->
        <div id="qcEventCountDown" class="col-6 col">
            <!-- ## DAYS ## -->
            <div class="dash days_dash">
                <div class="dash_title">dies</div>
                <div class="digits clearfix">
                    <!-- 	<div class="digit digit-1">0</div> -->
                    <div class="digit digit-2">1</div>
                    <div class="digit digit-3">6</div>
                </div>
            </div>
            <!-- ## HOURS ## -->
            <div class="dash hours_dash">
                <div class="dash_title">hores</div>
                <div class="digits clearfix">
                    <div class="digit digit-1">1</div>
                    <div class="digit digit-2">2</div>
                </div>
            </div>
            <!-- ## MINUTES ## -->
            <div class="dash minutes_dash">
                <div class="dash_title">minuts</div>
                <div class="digits clearfix">
                    <div class="digit digit-1">4</div>
                    <div class="digit digit-2">9</div>
                </div>
            </div>
            <!-- ## SECONDS ## -->
            <div class="dash seconds_dash">
                <div class="dash_title">segons</div>
                <div class="digits clearfix">
                    <div class="digit digit-1">2</div>
                    <div class="digit digit-2">0</div>
                </div>
            </div>
        </div>

        <!-- ## EVENT BANNER ## -->
        <div id="qcEventBanner" class="col-6 col">
            <ul>
                <li><b>PRO/1 </b> Siluetes morfològiques.  Aprendrem a identificar-nos,  conèixer-nos i triomfar!</li>
                <li><b>PRO/2</b> Colors.  Descobrirem quins colors ens il·luminen.</li>
                <li><b>PRO/3</b> Com portar les tendències que et faran brillar aquestes festes.</li>
                <li><b>PRO/4</b> Shopping  com anar a comprar i encertar-la. Trucs  per  no cometre errors.</li>
            </ul>
        </div>

    </div>
</header>
<!-- ## HEADER END ## -->

<!-- ## FULLSCREEN SLIDES ## -->
<section id="slideContent">

    <!-- ## SLIDE CONTROLS ## -->
    <div id="qcHomeSlideControls">

        <!-- ## SLIDE DOT NAV ## -->
        <ul id="slide-list"></ul>

        <!-- ## PROGRESS BAR ## -->
        <div id="progress-back" class="load-item">
            <div id="progress-bar"></div>
        </div>

    </div>

</section>
<!-- ## FULLSCREEN SLIDES ## -->

<!-- ## BACKGROUND OVERLAY ## -->
<section id="qcOverlay">
    <div id="qcEventLogo" class="loading">
        <a href="#">
            <img src="<?= base_url() ?>img/placeholder/center-logo.png" alt="" />
        </a>
    </div>
</section>
<!-- ## LOAD JAVASCRIPTS ## -->
<script src="<?= base_url() ?>js/template/2.1.1.jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/supersized.3.2.7.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/countdown.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/jquery.marquee.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/library.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/init.js" type="text/javascript"></script>
