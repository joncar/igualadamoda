<?php $this->load->view('includes/template/header',array('current'=>'workshops')); ?>
<!-- ## CONTENT WRAPPER ## -->
<div id="qcContentWrapper">

    <!-- ## PAGE TITLE ## -->
    <section id="qcSecbar">
        <div class="qcContainer">
            <h1>Workshop. <span>  La teva imatge és el teu èxit</span></h1>
        </div>
    </section>

    <!-- ## PAGE CONTENT ## -->
    <section id="qcContent">
        <div class="qcContainer">

            <!-- ## MOB NAV ## -->
            <div id="qcMbTrigger"></div>

            <!-- ## TABS ## -->
            <div id="qcTabs" class="tabs">

                <!-- ## TAB NAV ## -->
                <ul id="qcTabNav" class="clearfix">
                    <li><a href="#tab-1"><i class="icon-book-open icon"></i> <span>Qué ès?</span></a></li>
                    <li><a href="#tab-2"><i class="icon-clock-1 icon"></i> <span>Workshop</span></a></li>
                    <!-- <li><a href="#tab-3"><i class="icon-clock-1 icon"></i> <span>Workshop 2</span></a></li> -->
                    <li><a href="#tab-4"><i class="icon-map icon"></i> <span>Adreça</span></a></li>
                    <li><a href="#tab-5"><i class="icon-picture icon"></i> <span>Galeria</span></a></li>
                </ul>


                <!-- ===============================================

                        PAGE 1 - ABOUT

                =============================================== -->
                <div id="tab-1" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="stretch row clearfix">

                        <!-- ## TAB TITLE & DESC ## -->
                        <div class="col-4 col">
                            <div class="qcTabTitle">
                                <h4>Qué ès?<span> LA TEVA IMATGE ÉS EL TEU ÈXIT
                                 </span></h4>
                            </div>
                            <p class="qcPageDesc">

                                És un workshop que es realitzarà el matí del dia 29 de desembre de 10 a 14 hores, on
                                aprendrem a potenciar els nostres punts forts, aprofundint en temes tan importants per a la nostra imatge com són la morfologia, (tipus de silueta que tenim), quins colors ens queden millor per saber quines peces són les millors per a nosaltres.
								<br><br>Sabrem quins secrets amaga cada peça de roba. Crearem looks fantàstics i comptarem amb la col·laboració de les marques que es comercialitzen als establiments de la nostra ciutat.  
								<br><br>Coneixerem trucs de com hem d'anar a comprar i comprar a les rebaixes, parlarem de moda i tendències reafirmant la nostra personalitat a l’hora de vestir i quines són les tendències que ens faran brillar aquestes festes. <br><br>

                               
                            </p>
                        </div>

                        <!-- ## SLIDER ## -->
                        <div class="col-8 col">
                            <div class="video box no-border">
                                <div class="qcSliderWrapper">
                                   <!-- ## EMBEDED VIDEO [ YOUTUBE, VIMEO ] ## -->
                                    <div class="qcFitVids">
                                        <iframe src="http://player.vimeo.com/video/18433850?title=0&amp;byline=0&amp;portrait=0&amp;color=a57566" width="400" height="225"></iframe>
                                    </div>
                                    <div style="position:relative;">
                                        <ul class="single-carousel owl-carousel">
    										<li>
                                                <!-- ## IMAGE SLIDE ## -->
                                                <img src="<?= base_url() ?>img/galeria/3.jpg" alt=""/>

                                            </li>
                                            <li>
                                                <!-- ## IMAGE SLIDE ## -->
                                                <img src="<?= base_url() ?>img/galeria/12.jpg" alt=""/>

                                            </li>
                                            <li>
                                                <!-- ## IMAGE SLIDE ## -->
                                                <img src="<?= base_url() ?>img/galeria/17.jpg" alt=""/>

                                            </li>
                                            </ul>
                                        <!-- ## SLIDER NEXT PREV ## -->
                                        <div class="qcPrevNext">
                                            <div class="qcPrev"><i class="icon-left-open-big"></i></div>
                                            <div class="qcNext"><i class="icon-right-open-big"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ## SLIDER END ## -->

                            </div>
                        </div>

                    </div>
                    

                    <div class="speakers row clearfix">

                        <!-- ## TAB TITLE & DESC ## -->
                        <div class="col-4 col">
                            <div class="qcTabTitle">
                                <h4>Cv Ponent<span></span></h4>
                            </div>
                            <p class="qcPageDesc">He realitzat el disseny i l’estilisme dels Saleros de la ciutat d'Igualada, del Cavaller portador de l’estendard i dels gegants de la ciutat. 
                                També, el vestuari d'un personatge Reial del bàsquet del Barça, el vestuari dels actors de l'espectacle "Ovni" de Ferrers Brother's  i el vestuari de l'espectacle "Caravan" de Tot Circ. <br><br>
                                He dissenyat nombrosos vestits de festa i de núvia. Compaginant tasques de docent de moda, estilisme i d’artesania en cuir a l'Escola d'Art Gaspar Camps d’Igualada, amb l’organització i realització de diferents desfilades: Escola d'Art,
                                Stradivarius, Vestir Club, Centre Òptica Anoia, Fira Nuvis, Nuvis Fest, Igualada Comerç i he estat assessora d'imatge i docent en diversos cursos de moda i estilisme.
                            </p>
                        </div>

                        <!-- ## SPEAKERS ## -->
                        <div class="col-8 col">
                            <div class="box no-border nopad">

                                <!-- ## SPEAKERS LIST ## -->
                                <ul class="3-col-carousel owl-carousel qcTeamCol row">
                                    <!-- ## SPEAKER 1 ## -->
                                    <li>
                                        <div class="small-box" style=" text-align: left">
                                            <img src="<?= base_url() ?>img/placeholder/t2.png" alt="Team-1" title="http://photodune.net/item/smart-teacher/856363?WT.ac=search_thumb&WT.oss_phrase=smart%20&WT.oss_rank=65&WT.z_author=Pressmaster"/>
                                            <h4>Susana Maldonado</h4>
                                            <p>Dissenyadora i estilista d'indumentària, Personal Shopper i assessora d'imatge</p>
                                            <ul class="qcGlyphSocial clearfix">
                                                <li><a href="https://www.facebook.com/susanna.maldonadomaldonado" class="tips" title="FACEBOOK"><i class="icon-facebook"></i></a></li>
<!--                                                <li><a href="#" class="tips" title="TWITTER"><i class="icon-twitter"></i></a></li>-->
<!--                                                <li><a href="#" class="tips" title="DRIBBBLE"><i class="icon-dribbble"></i></a></li>-->
<!--                                                <li><a href="#" class="tips" title="PINTEREST"><i class="icon-pinterest"></i></a></li>-->
<!--                                                <li><a href="#" class="tips" title="LINKEDIN"><i class="icon-linkedin"></i></a></li>-->
                                            </ul>
                                        </div>
                                    </li>                                    
                            </div>
                        </div>
                        <!-- ## SPEAKERS END ## -->

                    </div>
                    <!-- ## ROW END ## -->

                    <!-- ## ROW ## -->
                    <div class="dblBorder">
                        <div class="row clearfix">
                            <div class="col-12 col">
                                <!-- ## MODULE TITLE ## -->
                                <div class="qcModTitle">
                                    <h1>Partners</h1>
                                    <p>Organitza</p>
                                </div>

                                <!-- ## SPONSORS LIST ## -->
                                <ul class="qcSposnsorList clearfix">
                                    <li>
                                        <a class="tips" title="Igualada Moda">
                                            <img src="<?= base_url() ?>img/light/partner-1.png" alt="" />
                                        </a>
                                    </li>
                                    <li>
                                        <a class="tips" title="Fagepi">
                                            <img src="<?= base_url() ?>img/light/partner-2.png" alt="" />
                                        </a>
                                    </li>

                                </ul>

                                <div class="qcModTitle">

                                    <p>Amb el suport</p>
                                </div>

                                <!-- ## SPONSORS LIST ## -->
                                <ul class="qcSposnsorList clearfix">
                                    <li>
                                        <a class="tips" title="Ajuntament d'Igualada">
                                            <img src="<?= base_url() ?>img/light/partner-3.png" alt="" />
                                        </a>
                                    </li>
                                    <li>
                                        <a class="tips" title="CCAM">
                                            <img src="<?= base_url() ?>img/light/partner-4.png" alt="" />
                                        </a>
                                    </li>
                                    <li>
                                        <a class="tips" title="Fitex">
                                            <img src="<?= base_url() ?>img/light/partner-5.png" alt="" />
                                        </a>
                                    </li>

                                </ul>

                                <div class="qcModTitle">

                                    <p>Col.labora</p>
                                </div>

                                <!-- ## SPONSORS LIST ## -->
                                <ul class="qcSposnsorList clearfix">
                                    <li>
                                        <a class="tips" title="Igualada Leather">
                                            <img src="<?= base_url() ?>img/light/partner-6.png" alt="" />
                                        </a>
                                    </li>

                                </ul>

                                <!-- ## SPONSERS LIST END ## -->
                            </div>
                        </div>
                    </div>
                    <!-- ## ROW END ## -->

                </div>
                <!-- ## PAGE 1 END ## -->



                <!-- ===============================================

                        PAGE 2 - SCHEDULE

                =============================================== -->
                <div id="tab-2" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="row clearfix">

                        <!-- ## TAB TITLE ## -->
                        <div class="col-4 col">
                            <div class="qcTabTitle no-border">
                                <h4>Programa<span> Matí de 10h a 14h</span></h4>
                            </div>
                        </div>

                        <!-- ## TAB DESC ## -->
                        <div class="col-8 col">
                            <div class="box no-border nopad">
                                <p class="qcPageDesc full"><b>1/ SILUETES MORFOLÒGIQUES.<BR></b>
                                 Aprendrem a potenciar la nostra imatge personal i a utilitzar el vestuari que més ens escau.
                                 </p>
                                 <p class="qcPageDesc full"><b>2/ COLORS.<BR></b>
                                 Descobrirem quins colors ens il·luminen 
                                 </p>
                                 <p class="qcPageDesc full"><b>3/ TENDÈNCIES.<BR></b>
                                 Com portar les tendències que et faran brillar aquestes festes
                                 </p>
                                 <p class="qcPageDesc full"><b>4/ SHOPPING.<BR></b>
                                 Com anar a comprar i encertar-la. Trucs  per  no cometre errors
                                 </p>
                                 <p class="qcPageDesc full"><b>ESMORZAR INCLÒS.<BR></b>
                                 
                                 </p>
                            </div>
                        </div>

                    </div>
                    <!-- ## ROW END ## -->

                    <!-- ## ROW ## -->
                    <div class="dblBorder">
                        <div class="row clearfix">
                            <div class="col-12 col">

                                <!-- ## SCHEDULE LIST ## -->
                                <div id="qcScheduleWrapper">

                                    <!-- ## DAY 1 ## -->
                                    <div class="qcScheduleDay">
                                        <header class="qcSchDay">
                                            <p>Dissabte<span>29 desembre</span></p>
                                        </header>
                                        <ul class="qcScheduleList clearfix">
                                            <li>
                                                <a href="javascript:void(0)" class="tips" title="MORFOLOGIES">
                                                    <img src="<?= base_url() ?>img/morfo.jpg" alt="" />
                                                    <div class="qcSchDesc">
                                                        <p class="qcSchTime">10:00 <sup>AM</sup></p>
                                                        <p class="qcSchSpeaker">Aprendrem a identificar-nos,  conèixer-nos i triomfar! <span> </span></p>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="tips" title="COLORS">
                                                    <img src="<?= base_url() ?>img/combinar.jpg" alt="" />
                                                    <div class="qcSchDesc">
                                                        <p class="qcSchTime">11:00 <sup>PM </sup></p>
                                                        <p class="qcSchSpeaker">Descobrirem quins colors ens il·luminen<span> </span></p>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="tips" title="TENDÈNCIES">
                                                    <img src="<?= base_url() ?>img/sostenible.jpg" alt="" />
                                                    <div class="qcSchDesc">
                                                        <p class="qcSchTime">12:00 <sup>AM</sup></p>
                                                        <p class="qcSchSpeaker">Com portar les tendències que et faran brillar aquestes festes<span> </span></p>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="tips" title="SHOPPING">
                                                    <img src="<?= base_url() ?>img/armari.jpg" alt="" />
                                                    <div class="qcSchDesc">
                                                        <p class="qcSchTime">13:00 <sup>PM </sup></p>
                                                        <p class="qcSchSpeaker">Com anar a comprar i encertar-la. Trucs  per  no cometre errors <span> </span></p>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                   
                                </div>
                                <!-- ## SCHEDULE LIST END ## -->

                            </div>
                        </div>
                    </div>


                </div>
                <!-- ## PAGE 2 END ## -->



                <!-- ===============================================

                        PAGE 3 - SPEAKERS

                =============================================== -->
                <div id="tab-3" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="row clearfix">

                        <!-- ## TAB TITLE ## -->
                        <div class="col-4 col">
                            <div class="qcTabTitle no-border">
                                <h4>W/S 2<span> Tarda de 16h a 19h</span></h4>
                            </div>
                        </div>

                        <!-- ## TAB DESC ## -->
                        <div class="col-8 col">
                            <div class="box no-border nopad">
                                <p class="qcPageDesc full"><b>ELS COLORS,
                                        <BR></b>
                                    LA TEVA IMATGE ÉS EL TEU ÈXIT<BR><BR></b>

                                    - El poder del color, com els identifico? - 
                                    <BR>
                                    - Els estampats. Quins em queden bé? - 
                                    <BR>
                                    - Aprendrem quins pentinats, joies ulleres s’escauen millor al nostre rostre -<BR>
                                    - Els teixits: textures, pesos i caients -<BR>
                                    - Visagisme: Correccions facials -<BR>
                                    - Definirem estils -<BR>
                                </p>
                            </div>
                        </div>

                    </div>
                    <!-- ## ROW END ## -->

                    <!-- ## ROW ## -->
                    <div class="dblBorder">
                        <div class="row clearfix">
                            <div class="col-12 col">

                                <!-- ## SCHEDULE LIST ## -->
                                <div id="qcScheduleWrapper">

                                    <!-- ## DAY 1 ## -->
                                    <div class="qcScheduleDay">
                                        <header class="qcSchDay">
                                            <p>Dissabte<span>11 novembre</span></p>
                                        </header>
                                        <ul class="qcScheduleList clearfix">
                                            <li>
                                                <a href="#" class="tips" title="PODER DEL COLOR">
                                                    <img src="<?= base_url() ?>img/color.jpg" alt="" />
                                                    <div class="qcSchDesc">
                                                        <p class="qcSchTime">16:00 <sup>AM</sup></p>
                                                        <p class="qcSchSpeaker">El poder del color<span> </span></p>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="tips" title="ESTAMPATS">
                                                    <img src="<?= base_url() ?>img/estampado.jpg" alt="" />
                                                    <div class="qcSchDesc">
                                                        <p class="qcSchTime">17:00 <sup>PM </sup></p>
                                                        <p class="qcSchSpeaker">Els estampats. Quins em queden bé? <span> </span></p>
                                                    </div>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#" class="tips" title="TEIXITS">
                                                    <img src="<?= base_url() ?>img/tejidos.jpg" alt="" />
                                                    <div class="qcSchDesc">
                                                        <p class="qcSchTime">18:00 <sup>PM </sup></p>
                                                        <p class="qcSchSpeaker">Els teixits: textures, pesos i caients <span> </span></p>
                                                    </div>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#" class="tips" title="VISAGISME">
                                                    <img src="<?= base_url() ?>img/visagisme.jpg" alt="" />
                                                    <div class="qcSchDesc">
                                                        <p class="qcSchTime">19:00 <sup>PM </sup></p>
                                                        <p class="qcSchSpeaker">Visagisme: Correccions facials <span> </span></p>
                                                    </div>
                                                </a>
                                            </li>


                                        </ul>
                                    </div>


                                </div>


                            </div>
                        </div>
                    </div>


                </div>
                <!-- ## PAGE 2 END ## -->



                <!-- ===============================================

                        PAGE 4 - VENUE

                =============================================== -->
                <div id="tab-4" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="row clearfix">

                        <!-- ## TAB TITLE ## -->
                        <div class="col-5 col">
                            <div class="qcTabTitle no-border">
                                <h4>On es fan els workshops?<span> Adoberia Bella. Barri del Rec</span></h4>
                            </div>
                        </div>

                        <!-- ## ADDRESS LIST ## -->
                        <div class="col-7 col">
                            <ul class="qcAddress">
                                <li><i class="icon-map"></i><p><strong>Adreça</strong> Adoberia Bella. C/Rec, 23/25 Cant. Joan Mercader. Igualada </p></li>
                                <li><i class="icon-user-1"></i><p><strong>Telèfon</strong><a title="telefon" href=" tel:938 032 993" target="_blank"> 938 032 993</a></p></li>
                                <!-- <li><i class="icon-print"></i><p><strong>FAX</strong>: +91-8097000001</p></li> -->
                                <li><i class="icon-mail-1"></i><p><strong>Email</strong> <a title="mail" href="mailto:info@igualadamoda.com" target="_blank">info@igualadamoda.com</a></p></li>
                                <li><i class="icon-globe-1"></i><p><strong>Web</strong> <a title="website" href="http://www.igualadamoda.com" target="_blank">www.igualadamoda.com</a></p></li>
                            </ul>
                        </div>

                    </div>
                    <!-- ## ROW END ## -->

                    <!-- ## ROW ## -->
                    <div class="dblBorder">
                        <div class="row clearfix">
                            <div class="col-12 col">

                                <!-- ## VENUE LAYOUT PLAN ## -->
                                <div class="qcEventlayout">
                                    <h1Fotos</h1>
                                        <a href="<?= base_url() ?>img/placeholder/layout.jpg" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/placeholder/layout.jpg" alt="" /> <br><br>
                                        </a>
                                        <a href="<?= base_url() ?>img/placeholder/layout1.jpg" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/placeholder/layout1.jpg" alt="" />
                                        </a>
                                </div>

                                <div class="dblBorder only"></div>

                                <!-- ## VENUE MAP ## -->
                                <div class="qcEventlayout">
                                    <h1>Mapa</h1>
                                    <div id="qcContactMap"></div>
                                    <div id="qcMapAddress" data-lat="41.5764857" data-lng="1.6143079999999372" data-add="Carrer del Rec, 25, 08700 Igualada, Barcelona"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- ## ROW END ## -->

                </div>
                <!-- ## PAGE 4 END ## -->



                <!-- ===============================================

                        PAGE 5 - GALLERY

                =============================================== -->
                <div id="tab-5" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="stretch row clearfix">

                        <!-- ## TAB TITLE & DESC ## -->
                        <div class="col-4 col">
                            <div class="qcTabTitle">
                                <h4>Galeria<span> Alguns exemples per la teva imatge</span></h4>
                            </div>
                            <p class="qcPageDesc">Aprendrem a potenciar la nostra imatge. Personal i a utilitzar el vestuari que més s’escau</p>
                        </div>

                        <!-- ## GALLERY LIST ## -->
                        <div class="col-8 col">
                            <div class="video box no-border">
                                <ul id="imgGallery" class="clearfix">
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/1.jpg"  data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/1.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/2.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/2.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/3.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/3.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/4.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/4.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/5.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/5.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/6.jpg" class="tips" title=""  data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/6.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/11.jpg" class="" title="UltraMusic Festival" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/11.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/8.jpg" class="" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/8.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/9.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/9.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/10.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/10.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/16.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/16.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/12.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/12.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/13.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/13.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/14.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/14.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/15.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/15.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/7.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/7.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/17.jpg" class="tips" title="" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/17.jpg" alt=""/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>img/galeria/18.jpg" class="tips" title="Excision" data-rel="prettyPhoto[pp_gal]">
                                            <img src="<?= base_url() ?>img/galeria/18.jpg" alt=""/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- ## GALLERY LIST END ## -->

                    </div>
                    <!-- ## ROW END ## -->

                </div>
                <!-- ## PAGE 5 END ## -->


            </div>
            <!-- ## TABS END ## -->

        </div>
    </section>
    <!-- ## PAGE CONTENT END ## -->


</div>
<!-- ## CONTENT WRAPPER END ## -->
<?php $this->load->view('includes/template/footer'); ?>
<script src="<?= base_url() ?>js/template/2.1.1.jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/library.js" type="text/javascript"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA&libraries=drawing,geometry,places"></script> 
<script src="<?= base_url() ?>js/template/init.js" type="text/javascript"></script>
