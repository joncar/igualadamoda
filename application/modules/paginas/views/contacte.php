<?php
    $talleres = $this->db->get_where('talleres',array('disponible'=>1));
?>
<?php $this->load->view('includes/template/header',array('current'=>'reserva')); ?>
<!-- ## CONTENT WRAPPER ## -->
<div id="qcContentWrapper">

    <!-- ## PAGE TITLE ## -->
    <section id="qcSecbar">
        <div class="qcContainer">
            <h1>Reserva. <span>La teva imatge és el teu èxit</span></h1>
        </div>
    </section>

    <!-- ## PAGE CONTENT ## -->
    <section id="qcContent">
        <div class="qcContainer">

            <!-- ## MOB NAV ## -->
            <div id="qcMbTrigger"></div>

            <!-- ## TABS ## -->
            <div id="qcTabs" class="tabs">

                <!-- ## TAB NAV ## -->
                <ul id="qcTabNav" class="clearfix">
                    <li>
                        <a href="#tab-1"><i class=""></i>
                            <i class="icon-mail-1 icon"></i> <span>Contacte</span>
                        </a>
                    </li>
                    <li>
                        <a href="#tab-4">
                            <i class="icon-ticket icon"></i> <span>Compra el teu workshop</span>
                        </a>
                    </li>
                </ul>


                <!-- ===============================================

                        PAGE 1 - TICKET

                =============================================== -->
                <div id="tab-1" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="row clearfix">

                        <!-- ## TAB TITLE ## -->
                        <div class="col-6 col" >
                            <div class="qcTabTitle no-border">
                                <h4>Contacte<span> Omple el següent formulari.</span></h4>
                            </div>
                        </div>

                        <!-- ## TAB DESC ## -->
                        <div class="col-6 col">
                            <ul class="qcAddress">
                                <li><i class="icon-map"></i><p><strong>Adreça</strong> Adoberia Bella. C/Rec, 23/25 Cant. Joan Mercader. Igualada </p></li>
                                <li><i class="icon-user-1"></i><p><strong>Telèfon</strong><a title="telefon" href=" tel:938 032 993" target="_blank"> 938 032 993</a></p></li>
                                <!-- <li><i class="icon-print"></i><p><strong>FAX</strong>: +91-8097000001</p></li> -->
                                <li><i class="icon-mail-1"></i><p><strong>Email</strong> <a title="mail" href="mailto:info@igualadamoda.com" target="_blank">info@igualadamoda.com</a></p></li>
                                <li><i class="icon-globe-1"></i><p><strong>Web</strong> <a title="website" href="http://www.igualadamoda.com" target="_blank">www.igualadamoda.com</a></p></li>
                            </ul>
                        </div>




                    </div>
                    <!-- ## ROW END ## -->

                    <!-- ## ROW ## -->
                    <div class="dblBorder">
                        <div class="row clearfix">
                            <div class="col-12 col">
                                <div class="qcTcktRegForm contact">
                                    <!-- ## CONTACT FORM ## -->
                                    <form action="#" name="qcContactForm" method="post" onsubmit="return contacto(this)">
                                        <ul class="clearfix">
                                            <li>
                                                <div class="wrapper">
                                                    <label for="contact-name">Nom</label>
                                                    <input type="text" id="contact-name" name="nombre" class="requiredField" value="" placeholder="Escriu el teu nom" />
                                                </div>
                                            </li>
                                            <li>
                                                <div class="wrapper">
                                                    <label for="contact-email">Email</label>
                                                    <input type="text" id="contact-email" name="email" class="email requiredField" value="" placeholder="Escriu el teu email" />
                                                </div>
                                            </li>
                                            <li>
                                                <div class="wrapper">
                                                    <label for="contact-phone">Telèfon</label>
                                                    <input type="text" id="contact-phone" name="telefono" class="requiredField" value="" placeholder="Escriu un número de contacte " />
                                                </div>
                                            </li>
                                            <li>
                                                <div class="wrapper">
                                                    <label for="contact-subject">Tema</label>

                                                    <textarea id="contact-tema" name="tema" class="requiredField" placeholder="De que vols parlar?"></textarea>
                                                    </select>
                                                </div>
                                            </li>
                                            <li class="full">
                                                <div class="wrapper">
                                                    <label for="contact-message">Missatge</label>
                                                    <textarea id="contact-message" name="mensaje" class="requiredField" placeholder="Escriu el teu missatge"></textarea>
                                                </div>
                                            </li>											
                                        </ul>
                                        <div class="qcTcktSubmit">                                            
                                            <div id='messageBoxContacto' style="display:none"></div>
                                            <button type="submit" class="submit">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ## ROW END ## -->

                </div>
                <!-- ## PAGE 1 END ## -->



                <!-- ===============================================

                        PAGE 4 - CONTACT

                =============================================== -->
                <div id="tab-4" class="qcTabPage clearfix">
                    
                    
                    <!-- ## ROW ## -->
                    <div class="">
                        <div class="row clearfix">
                            <div class="col-12 col">
                                <!-- ## MODULE TITLE ## -->
                                <div class="qcModTitle">
                                    <h1>Preus</h1>
                                    <p>Continguts del works i els seus preus</p>
                                </div>
                                <!-- ## PRICING ## -->
                                <div class="qcPricingWrapper clearfix">
                                    <?php foreach($talleres->result() as $t): ?>
                                        <!-- ## PRICING 1 ## -->
                                        <div class="qcPricing col-12 col">
                                            <div class="box">
                                                <header style="line-height:31px;padding-top: 31px; padding-bottom: 15px"><?= $t->titulo ?> <header style=" font-size: 18px; line-height: 20px; padding-bottom: 20px; padding-left:7px;padding-right:7px; font-weight: 400;"><?= $t->descripcion_corta ?> </header></header>

                                                <div class="price"><span><?= $t->precio ?>€</span> per persona</div>
                                                <?= $t->descripcion ?>
                                                <footer id="footer<?= $t->id ?>" class="footeritem"><a href="javascript:seleccionar(<?= $t->id ?>,'<?= $t->titulo_formulario ?>','<?= $t->precio ?>')">COMPRA ARA</a></footer>
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                                <!-- ## PRICING ## -->
                            </div>
                        </div>
                    </div>
                    <!-- ## ROW END ## -->
                    
                    <!-- ## ROW ## -->
                    <div class="dblBorder ticket row clearfix" id="formularioreserva">

                        <!-- ## TAB TITLE & DESC ## -->
                        <div class="col-4 col" >
                            <div class="qcTabTitle no-border">
                                <h4>Compra'l ara!<span> No et quedis sense poder assistir-hi</span></h4>
                                <p class="qcPageDesc full">Omple el formulari amb totes les dades que et demanem i fes una transferència bancària al número de compte: <b style=" font-size: 15px"><br>BANKINTER<br>ES31-0128-0536-930500001184</b><!-- <br>indicant com a referència el/s workshop/s que haguis escollit i el teu nom complert, quan el pagament sigui efectiu, t'enviarem un email amb la teva entrada per assitir al/s workshop que haguis escollit. -->
                                    <!-- <br><br>Aquestes jornades es realitzaran matí i tarda i són independents una de l’altra, de manera que pots assistir-hi al matí i/o a la tarda --></p>
                            </div>
                        </div>

                        <!-- ## TICKET ## -->
                        <div class="col-8 col">
                            <div class="box no-border nopad">
                                <div class="qcTcktRegForm">

                                    <!-- ## MODULE TITLE ## -->
                                    <div class="qcModTitle">
                                        <h1>Dades de reserva</h1>
                                        <p>Omple el següent formulari.</p>
                                    </div>

                                    <!-- ## TICKET FORM ## -->
                                    <form action="#" onsubmit="return send(this)" name="qcTcktForm" method="post">
                                        <ul class="clearfix">
                                            <li>
                                                <div class="wrapper">
                                                    <label for="name">Nom</label>
                                                    <input type="text" id="name" name="nombre" class="requiredField" value="" placeholder="Escriu el teu nom" />
                                                </div>
                                            </li>
                                            <li>
                                                <div class="wrapper">
                                                    <label for="email">Email</label>
                                                    <input type="text" id="email" name="email" class="email requiredField" value="" placeholder="Escriu el teu email" />
                                                </div>
                                            </li>
                                            <li>
                                                <div class="wrapper">
                                                    <label for="phone">Telèfon</label>
                                                    <input type="text" id="phone" name="telefono" class="requiredField" value="" placeholder="Escriu un número de contacte" />
                                                </div>
                                            </li>
                                            <li>
                                                <div class="wrapper herr">
                                                    <label for="ticket">Workshops</label>
                                                    <select name="talleres_id" id="talleres_id">
                                                        <option value="">Selecciona el teu workshop</option>
                                                        <?php foreach($talleres->result() as $t): ?>
                                                            <option value="<?= $t->id ?>" data-precio='<?= $t->precio ?>'><?= $t->titulo_formulario ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <input type="hidden" name="taller" id="taller" value="">
                                                    <select name="cantidad" id="cantidad" class="requiredField">                                                        
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                    </select>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="qcTcktCheck">
<!--                                            <input type="checkbox" name="condiciones" value="Book Ticket" class="requiredField checkbox" />-->
                                            <span style="color: #777;">Has de fer la transferència de </span><span style="color: #777; font-size:40px;" id="precio"> 0€</span><br><br><br>
                                            <div class="dblBorder ticket row clearfix" style=" padding-top: 20px"></div><span style="color: #777;font-family: "Montserrat", Helvetica, Arial, sans-serif">Comptes on has de fer la transferència: <br><br>BANKINTER ES31-0128-0536-930500001184</span><span style="color: #777; font-size:40px;"</span>
                                        </div>

                                        <div class="qcTcktSubmit">
                                            <div id='messageBox' style="display:none"></div>
                                            <button type="submit" class="submit">Reserva Workshop</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- ## TICKET FORM END ## -->
                            </div>
                        </div>
                        <!-- ## TICKET END ## -->

                    </div>
                    <!-- ## ROW END ## -->


                </div>
                <!-- ## PAGE 4 END ## -->



            </div>
            <!-- ## TABS END ## -->

        </div>
    </section>
    <!-- ## PAGE CONTENT END ## -->


</div>
<!-- ## CONTENT WRAPPER END ## -->
<?php $this->load->view('includes/template/footer'); ?>
<!-- ## LOAD JAVASCRIPTS ## -->
<script src="<?= base_url() ?>js/template/2.1.1.jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/library.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoW1mnkomGhsB2yL--AYoFdnE-jkgskSI" type="text/javascript"></script>
<script src="<?= base_url() ?>js/template/init.js" type="text/javascript"></script>
<script>
    function seleccionar(id,titulo,precio){
        var precio = parseFloat(precio);
        var total = precio * parseInt($("#cantidad").val());
        total = isNaN(total)?0:total;
        $("#precio").html(total.toFixed(0)+'€');
        $(".footeritem").removeClass('active');
        $("#footer"+id).addClass('active');
        $("#taller").val(titulo);
        $("#talleres_id").val(id);
        $("html, body").animate({scrollTop:parseInt($("#formularioreserva").offset().top)-100},500,'swing');
    }
    
    $("#talleres_id, #cantidad").change(function(){
        $("#taller").val($("#talleres_id").find('option:selected').html());
        var precio = parseFloat($("#talleres_id").find('option:selected').data('precio'));
        var total = precio * parseInt($("#cantidad").val());
        total = isNaN(total)?0:total;
        $("#precio").html(total.toFixed(0)+'€');
    });
    
    function send(form){
        var datos = new FormData(form);
        $.ajax({
            url: '<?= base_url('reservas/frontend/reservas/insert_validation') ?>',
            data: datos,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                data = $(data).text();
                data = JSON.parse(data);
                if(data.success){
                    $.ajax({
                        url: '<?= base_url('reservas/frontend/reservas/insert') ?>',
                        data: datos,
                        context: document.body,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success:function(data){
                            data = $(data).text();
                            data = JSON.parse(data);
                            if(data.success){
                                $("#messageBox").html("La seva sol·licitud s'ha enviat amb èxit, ja pots fer la transferència bancaria");
                            }else{
                                $("#messageBox").html("Si us plau verifiqui totes les dades, abans d'enviar");
                            }
                            $("#messageBox").show();

                        }
                    });
                }else{
                    $("#messageBox").html(data.error_message);
                    $("#messageBox").show();
                }
                
            }
        });
        return false;
    }
    
    function contacto(form){
        var datos = new FormData(form);
        $.ajax({
            url: '<?= base_url('paginas/frontend/contacto') ?>',
            data: datos,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                $("#messageBoxContacto").html(data);
                $("#messageBoxContacto").show();
            }
        });
        return false;
    }
    
</script>
