<!-- ## FOOTER ## -->
<footer id="qcFooter" class="clearfix">
    <div class="qcContainer">

        <!-- ## FOOTER NAV ## -->
        <div class="col-5 col">
            <nav id="qcFooterNav">
                <ul class="clearfix">
                    <li><a href="<?= site_url() ?>">Inici</a></li>
                    <li><a href="<?= site_url('p/workshops') ?>">Workshop</a></li>
                    <li><a href="<?= site_url('p/contacte') ?>">Contacte</a></li>
                    <li><a href="<?= site_url('p/reserva') ?>">Reserva</a></li>
                </ul>
            </nav>
        </div>

        <!-- ## FOOTER LOGO ## -->
        <div id="qcFooterLogo" class="col-2 col">
            <a href="#">
                <img src="<?= base_url() ?>img/placeholder/logo-footer.png" alt="LOGO" />
            </a>
        </div>

        <!-- ## FOOTER COPYRIGHTS ## -->
        <div id="qcFooterPara" class="col-5 col">
            <p>Jornades de Moda i imatge. La teva imatge és el teu èxit <br /> <span>&copy;Copyright Igualada Moda 2018</span></p>
        </div>

    </div>
</footer>
<!-- ## FOOTER END ## -->
