<header id="qcHomeHeader">
    <div class="row">

        <!-- ## LOGO ## -->
        <div id="qcLogo" class="col-6 col">
            <a href="<?= site_url() ?>"><img src="<?= base_url() ?>img/placeholder/logo-header.png" alt="" /></a>
        </div>

        <!-- ## SITE NAVIGATION ## -->
        <nav id="qcPriNav" class="col-6 col">
            <ul class="clearfix">
                <li <?= @$current=='workshops'?'class="current"':'' ?>><a href="<?= site_url('p/workshops') ?>"><i class="icon-calendar-2 icon"></i> <span>WORKSHOP</span></a></li>
                <li <?= @$current=='reserva'?'class="current"':'' ?>><a href="<?= site_url('p/reserva') ?>"><i class="icon-ticket icon"></i> <span>RESERVA</span></a></li>
            </ul>
        </nav>

    </div>
</header>
