<!doctype html>
<html>
    <head>
        <!-- ## SITE META ## -->
        <meta charset="utf-8">
        <title><?= empty($title) ? 'Igualada Moda' : $title ?></title>
        <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
        <meta name="description" content="<?= empty($keywords) ?'': $description ?>" />     
        <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>    
        <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="<?= base_url() ?>img/placeholder/favicon.ico">

        <!-- ## LOAD STYLSHEETS ## -->
        <link rel="stylesheet" media="all" href="<?= base_url() ?>css/template/style.css"/>

        <!-- ## GOOGLE FONTS ## -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link rel="stylesheet" media="all" href="<?= base_url() ?>css/jquery.selectBox.css"/>

        <!-- ## FULLSCREEN SLIDESHOW ## -->
        <script type="text/javascript">
            function slide_fullscreen() {
                jQuery(function ($) {
                    $.supersized({
                        // Functionality
                        slide_interval: 8000,
                        transition: 1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                        transition_speed: 1000,
                        // Components
                        slide_links: 'blank', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
                        progress_bar: 1,
                        slides: [
                            {image: src = "<?= base_url() ?>img/11.jpg"
                            },
                            {image: src = "<?= base_url() ?>img/full-bg-4.jpg"
                            }
                        ]
                    });
                });
            }
            window.onload = slide_fullscreen;
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109182105-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-109182105-1');
        </script>

    </head>
    <body>        
        <?php $this->load->view($view); ?>        
    </body>

</html>
